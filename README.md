# ip-php

Return IP address for the questioner

## Configuration

Stuff can be configured via environment variables

Env var | Default | Notes
------- | ------- | -----
`IP_SOURCE_VAR` | `"REMOTE_ADDR"` | Variable to hold the IP of the client. When behind a proxy, one could change this to `"HTTP_X_FORWARDED_FOR"` for example.
`IP_RESOLVE` | `false` | Resolve IP address on the second line
