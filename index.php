<?php
header('Content-Type: text/plain; charset=utf-8');
$source = getenv('IP_SOURCE_VAR') ?: 'REMOTE_ADDR';
$resolve = filter_var(getenv('IP_RESOLVE'), FILTER_VALIDATE_BOOLEAN) ?: false;

$ip = getenv($source);

echo "$ip\n" ?: '';
if ($ip && $resolve) {
  echo gethostbyaddr($ip)."\n";
}
